@ECHO OFF
:: @AUTHOR JOHN MELODY ME <J.CHENG@SLEEKFLOW.IO>
:: THIS IS TO CONFIGURE FLUTTER VERSION MANAGER INTO THE WINDOWS 10 
:: SYSTEM
:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
::  SET THE ENVIROMENT TO CURRENT
SETLOCAL EnableDelayedExpansion
:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
TITLE [SLEEKFLOW] CONFIGURE FLUTTER ENVIRONMENT
:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
ECHO PLEASE WAIT... CHECKING SYSTEM REQUIREMENTS...
:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
FOR /F "tokens=4-5 delims=. " %%i IN ('ver') DO  SET VERSION=%%i.%%j
::
REM INSPECT WINDOWS VERSION
IF "%version%" == "11.0" CALL SET ENV = OK
IF "%version%" == "10.0" CALL SET ENV = OK
IF "%version%" == "6.3" CALL SET ENV = OK
IF "%version%" == "6.2" CALL SET ENV = OK
IF "%version%" == "6.1" CALL SET ENV = OK
IF "%version%" == "6.0" CALL SET ENV = NOT SUPPORTED
:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
TIMEOUT 3
:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
SET ENV
:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
ECHO OFF !ENV!
:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
:: INSTALL CHOCOLATEY
@"%SystemRoot%\System32\WindowsPowerShell\v1.0\powershell.exe" -NoProfile -InputFormat None -ExecutionPolicy Bypass -Command "iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))" && SET "PATH=%PATH%;%ALLUSERSPROFILE%\chocolatey\bin"
:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
TIMEOUT 3
:: INSTALL FLUTTER VERSION MANAGER (FVM)
choco install fvm
:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
TIMEOUT 3
:: ACTIVATE FVM VIA DART COMPILER
dart pub global activate fvm
:: REMOVE FLUTTER FROM LOCAL ENVIRONMENT
SETX flutter
:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
TIMEOUT 3
:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
ECHO OFF VISIT https://fvm.app/docs/getting_started/configuration/
:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
PAUSE
ENDLOCAL