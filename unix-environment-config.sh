# /usr/bin/env bash
# @authour John Melody Me <J.cheng@sleekflow.io>
# This is to confgure Flutter Version Manager
# for Unix system `freeBSD, any Linux distro,
# and MacOS `
local_flutter="flutter"
local_homebrew="brew"
flutter_version_manager="fvm"

package_ok=$(dpkg-query -W --showformat='${Status}\n' $local_flutter|grep "install ok installed") 
package_ok=$(dpkg-query -W --showformat='${Status}\n' $local_homebrew|grep "install ok installed") 
package_ok=$(dpkg-query -W --showformat='${Status}\n' $flutter_version_manager|grep "install ok installed") 

echo Checking for $local_flutter: $package_ok
echo Checking for $local_homebrew: $package_ok
echo Checking for $flutter_version_manager: $package_ok

if ["" = "$package_ok"]; then
	echo "No $local_flutter. Setting up $local_flutter."
	echo "No $local_homebrew. Setting up $local_homebrew."
	echo "No $flutter_version_manager. Setting up $flutter_version_manager."

	# Install HOMEBREW
	if [["uname" == "Linux" ]]; then
		test -d ~/.linuxbrew && eval "$(~/.linuxbrew/bin/brew shellenv)"
		test -d /home/linuxbrew/.linuxbrew && eval "$(/home/linuxbrew/.linuxbrew/bin/brew shellenv)"
		test -r ~/.bash_profile && echo "eval \"\$($(brew --prefix)/bin/brew shellenv)\"" >> ~/.bash_profile
		echo "eval \"\$($(brew --prefix)/bin/brew shellenv)\"" >> ~/.profile
	fi

	case "$OSTYPE" in
		darwin*)  sudo brew tap leoafarias/fvm && sudo brew install fvm && dart pub global activate fvm ;; 
		linux*)   sudo apt-get install fvm && dart pub global activate fvm;;
		bsd*)     sudo pkg install fvm && dart pub global activate fvm;;
		*)        echo "unknown: $OSTYPE" ;;
	esac
fi